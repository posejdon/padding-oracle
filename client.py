# coding: utf-8

from Crypto.Cipher import AES
import os,socket,threading
import base64, math, binascii
import StringIO

iv = 'This is an IV456'
local_ip = socket.gethostbyname(socket.gethostname())
local_port = 8887
connect_port = 8888
block_size = 16

class client():
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((local_ip,local_port))

    def run(self):
        self.sock.connect((local_ip, connect_port))
        while 1:
            data = raw_input('Input data:\n')
            self.sock.send(data)
            reply = self.sock.recv(4096)
            print(reply)
            try:
                print(base64.b64decode(reply))
            except:
                pass
    
    def padOracleAtk(self):
        self.sock.connect((local_ip, connect_port))
        self.sock.send('iv')
        self.sock.recv(4096)
        self.sock.send(iv)
        self.sock.recv(4096)
        answer = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel nisi in lectus dictum congue vitae quis velit. Quisque fringilla metus vulputate justo luctus tempus. Pellentesque eu nulla mi. Integer ultricies eros et justo malesuada vehicula. Sed nec ornare ipsum. Nulla facilisi. Suspendisse potenti. Fusce quis sodales ipsum. Nam molestie nunc et consequat lacinia. Maecenas ut velit gravida felis malesuada scelerisque. Ut euismod, velit a porttitor porttitor, mi tellus cursus quam, ut rutrum neque sem a turpis. Ut sollicitudin eros sit amet urna suscipit interdum. Quisque porta quis purus at tincidunt.'
        self.sock.send('e')
        self.sock.recv(4096)
        self.sock.send(answer)
        cipherTxt64 = self.sock.recv(4096)
        cipherTxt = base64.b64decode(cipherTxt64)
        cipherBytes = bytearray(cipherTxt)
        
        print(cipherTxt)
        
        intermediate_state = []
        
        blocks = len(cipherBytes) / block_size
        
        if blocks >= 2:
            intermediate_state = self.attackLastByte(cipherBytes)
            if blocks > 2:
                for i in range(1, blocks-1):
                    print(len(cipherBytes[:-(i*block_size)]))
                    intermediate_state =  self.attackLastByte(cipherBytes[:-(i*block_size)]) + intermediate_state
        print(block_size)
        intermediate_state = self.attackIV(cipherBytes[:block_size]) + intermediate_state
        
        decrypted = []
        iv_bytes = bytearray(iv)
        
        for i in range(block_size):
            char = iv_bytes[i] ^ intermediate_state[i]
            decrypted.append(chr(char))
        for i in range(len(intermediate_state)-block_size):
            char = cipherBytes[i] ^ intermediate_state[i+block_size]
            decrypted.append(chr(char))
        print(''.join(decrypted))
                    
        self.sock.close()
    
    def attackLastByte(self, cipherBytes):
        intermediate_state = []
        _c = bytearray(cipherBytes[:-(block_size*2)] + bytearray([0]*block_size) + cipherBytes[-(block_size):])
        for i in range(1, block_size+1):
            for j in range(1, i):
                _c[-block_size-j] = intermediate_state[j-1] ^ i
            j = 0
            while 1:
                _c[-block_size-i] = j
                self.sock.send('d')
                self.sock.recv(4096)
                self.sock.send(base64.b64encode(_c))
                reply = self.sock.recv(4096)
                if reply != 'ERROR':
                    intermediate_state.append(j ^ i)
                    break
                j += 1
        return intermediate_state[::-1]
        
    def attackIV(self, cipherBytes):
        intermediate_state = []
        nulls = ''
        _c = bytearray(cipherBytes)
        for i in range(1, block_size+1):
            nulls = '\x00'*(block_size-i)
            padding = ''
            for j in range(1, i):
                padding = chr((intermediate_state[j-1] ^ i)) + padding
            j = 0
            while 1:
                self.sock.send('iv')
                self.sock.recv(4096)
                if j > 255:
                    raise ValueError('j > 255')
                self.sock.send(nulls + chr(j) + padding)
                
                self.sock.recv(4096)
                self.sock.send('d')
                self.sock.recv(4096)
                self.sock.send(base64.b64encode(_c))
                reply = self.sock.recv(4096)
                if reply != 'ERROR':
                    intermediate_state.append(j ^ i)
                    break
                j += 1
        self.sock.send('iv')
        self.sock.recv(4096)
        self.sock.send(iv)
        self.sock.recv(4096)
        return intermediate_state[::-1]
        

    def stop(self):
        self.sock.close()

client().padOracleAtk()